class UserModel{

  int _id;
  String _nickname;
  String _email;
  String _password;

  UserModel({int id,String nickname,String email,String pass}){
    this._id = id;
    this._nickname= nickname;
    this._email= email;
    this._password= pass;
  }

  UserModel.fromMap(Map<String, dynamic> map){
    this._id = map['id'];
    this._nickname = map['nickname'];
    this._email = map['email'];
    this._password = map['password'];
  }

  Map<String, dynamic> toMap(){
    return {
      'id': this._id,
      'nickname': this._nickname,
      'email': this._email,
      'password': this._password
    };
  }
}